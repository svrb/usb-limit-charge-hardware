EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Relay_SolidState:MOC3021M U2
U 1 1 6120AA30
P 5550 2850
F 0 "U2" H 5550 3175 50  0000 C CNN
F 1 "MOC3021M" H 5550 3084 50  0000 C CNN
F 2 "Package_DIP:DIP-6_W7.62mm_LongPads" H 5350 2650 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/MOC3023M-D.PDF" H 5550 2850 50  0001 L CNN
	1    5550 2850
	1    0    0    -1  
$EndComp
$Comp
L Triac_Thyristor:BT136-500 Q1
U 1 1 6120C4AD
P 6800 2900
F 0 "Q1" H 6928 2946 50  0000 L CNN
F 1 "BT136-500" H 6200 2950 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7000 2825 50  0001 L CIN
F 3 "http://www.micropik.com/PDF/BT136-600.pdf" H 6800 2900 50  0001 L CNN
	1    6800 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 6120F0F2
P 6250 2650
F 0 "R2" V 6250 2600 50  0000 L CNN
F 1 "330" V 6150 2550 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6180 2650 50  0001 C CNN
F 3 "~" H 6250 2650 50  0001 C CNN
	1    6250 2650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 6120F4D9
P 6250 3100
F 0 "R3" V 6250 3100 50  0000 C CNN
F 1 "330" V 6350 3100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6180 3100 50  0001 C CNN
F 3 "~" H 6250 3100 50  0001 C CNN
	1    6250 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 6120FE65
P 4950 2750
F 0 "R1" V 4950 2750 50  0000 C CNN
F 1 "470" V 4834 2750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4880 2750 50  0001 C CNN
F 3 "~" H 4950 2750 50  0001 C CNN
	1    4950 2750
	0    1    1    0   
$EndComp
$Comp
L Interface_USB:CH340G U1
U 1 1 61210A9D
P 4250 2800
F 0 "U1" H 4350 2200 50  0000 C CNN
F 1 "CH340G" H 4450 2100 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W10.16mm" H 4300 2250 50  0001 L CNN
F 3 "http://www.datasheet5.com/pdf-local-2195953" H 3900 3600 50  0001 C CNN
	1    4250 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 61214672
P 3600 2200
F 0 "C3" V 3348 2200 50  0000 C CNN
F 1 "100nf" V 3439 2200 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 3638 2050 50  0001 C CNN
F 3 "~" H 3600 2200 50  0001 C CNN
	1    3600 2200
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 612157C8
P 3150 2950
F 0 "C1" V 3200 3050 50  0000 C CNN
F 1 "22pf" V 3300 3050 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 3188 2800 50  0001 C CNN
F 3 "~" H 3150 2950 50  0001 C CNN
	1    3150 2950
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 612166A7
P 3150 3250
F 0 "C2" V 3200 3350 50  0000 C CNN
F 1 "22pf" V 3300 3250 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 3188 3100 50  0001 C CNN
F 3 "~" H 3150 3250 50  0001 C CNN
	1    3150 3250
	0    1    1    0   
$EndComp
$Comp
L power:GNDREF #PWR04
U 1 1 612206CD
P 4250 3400
F 0 "#PWR04" H 4250 3150 50  0001 C CNN
F 1 "GNDREF" H 4255 3227 50  0001 C CNN
F 2 "" H 4250 3400 50  0001 C CNN
F 3 "" H 4250 3400 50  0001 C CNN
	1    4250 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR01
U 1 1 612210A4
P 2950 2800
F 0 "#PWR01" H 2950 2550 50  0001 C CNN
F 1 "GNDREF" H 2955 2627 50  0001 C CNN
F 2 "" H 2950 2800 50  0001 C CNN
F 3 "" H 2950 2800 50  0001 C CNN
	1    2950 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR02
U 1 1 6122157C
P 3000 3250
F 0 "#PWR02" H 3000 3000 50  0001 C CNN
F 1 "GNDREF" H 3005 3077 50  0001 C CNN
F 2 "" H 3000 3250 50  0001 C CNN
F 3 "" H 3000 3250 50  0001 C CNN
	1    3000 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR03
U 1 1 61221BE7
P 3450 2200
F 0 "#PWR03" H 3450 1950 50  0001 C CNN
F 1 "GNDREF" H 3455 2027 50  0001 C CNN
F 2 "" H 3450 2200 50  0001 C CNN
F 3 "" H 3450 2200 50  0001 C CNN
	1    3450 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR05
U 1 1 61221E9B
P 5250 2950
F 0 "#PWR05" H 5250 2700 50  0001 C CNN
F 1 "GNDREF" H 5255 2777 50  0001 C CNN
F 2 "" H 5250 2950 50  0001 C CNN
F 3 "" H 5250 2950 50  0001 C CNN
	1    5250 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 2750 7250 2650
Wire Wire Line
	6100 2650 5850 2650
Wire Wire Line
	5850 2650 5850 2750
Wire Wire Line
	5850 2950 6100 2950
Wire Wire Line
	7250 3100 7250 2850
Wire Wire Line
	6650 3000 6100 3000
Wire Wire Line
	6800 3050 6800 3100
Wire Wire Line
	6800 2750 6800 2650
Wire Wire Line
	5250 2750 5100 2750
Wire Wire Line
	4800 2750 4800 3100
Wire Wire Line
	4800 3100 4650 3100
Wire Wire Line
	4150 2200 3750 2200
Wire Wire Line
	3250 2200 3300 2200
Wire Wire Line
	3300 2200 3300 2150
Wire Wire Line
	3300 2150 4250 2150
Wire Wire Line
	4250 2150 4250 2200
Wire Wire Line
	3250 2400 3750 2400
Wire Wire Line
	3750 2400 3750 2700
Wire Wire Line
	3750 2700 3850 2700
Wire Wire Line
	3250 2500 3650 2500
Wire Wire Line
	3650 2500 3650 2800
Wire Wire Line
	3650 2800 3850 2800
Wire Wire Line
	3850 3000 3850 2950
Wire Wire Line
	3850 3200 3850 3250
Wire Wire Line
	3000 2950 3000 3250
Wire Wire Line
	6100 2950 6100 3000
Wire Wire Line
	6400 3100 6800 3100
Wire Wire Line
	6400 2650 6800 2650
$Comp
L Device:Crystal Y1
U 1 1 6121319E
P 3550 3100
F 0 "Y1" V 3550 3150 50  0000 R CNN
F 1 "12mhz" V 3550 3450 50  0000 R CNN
F 2 "Crystal:Crystal_HC49-4H_Vertical" H 3550 3100 50  0001 C CNN
F 3 "~" H 3550 3100 50  0001 C CNN
	1    3550 3100
	0    1    1    0   
$EndComp
Connection ~ 6100 3000
Wire Wire Line
	6100 3000 6100 3100
Connection ~ 6800 3100
Wire Wire Line
	6800 3100 7250 3100
Connection ~ 6800 2650
Wire Wire Line
	6800 2650 7250 2650
$Comp
L Connector:USB_A J1
U 1 1 6121D26B
P 2950 2400
F 0 "J1" H 3007 2867 50  0000 C CNN
F 1 "USB_A" H 3007 2776 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H 3100 2350 50  0001 C CNN
F 3 " ~" H 3100 2350 50  0001 C CNN
	1    2950 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2950 3550 2950
Wire Wire Line
	3300 3250 3550 3250
Connection ~ 3550 2950
Wire Wire Line
	3550 2950 3850 2950
Connection ~ 3550 3250
Wire Wire Line
	3550 3250 3850 3250
$Comp
L Device:R R4
U 1 1 612D94C3
P 3400 1750
F 0 "R4" V 3193 1750 50  0000 C CNN
F 1 "470" V 3284 1750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 3330 1750 50  0001 C CNN
F 3 "~" H 3400 1750 50  0001 C CNN
	1    3400 1750
	0    1    1    0   
$EndComp
$Comp
L Device:LED D1
U 1 1 612DA32B
P 3750 1750
F 0 "D1" H 3743 1495 50  0000 C CNN
F 1 "LED" H 3743 1586 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 3750 1750 50  0001 C CNN
F 3 "~" H 3750 1750 50  0001 C CNN
	1    3750 1750
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 612DF2DC
P 4800 2450
F 0 "R5" H 4870 2496 50  0000 L CNN
F 1 "470" H 4870 2405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" V 4730 2450 50  0001 C CNN
F 3 "~" H 4800 2450 50  0001 C CNN
	1    4800 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 612DF2E2
P 4950 2200
F 0 "D2" H 4943 1945 50  0000 C CNN
F 1 "LED" H 4943 2036 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 4950 2200 50  0001 C CNN
F 3 "~" H 4950 2200 50  0001 C CNN
	1    4950 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	4800 2500 4800 2600
Wire Wire Line
	4800 2600 4800 2750
Connection ~ 4800 2600
Connection ~ 4800 2750
Wire Wire Line
	4800 2300 4800 2200
$Comp
L power:GNDREF #PWR07
U 1 1 612E5688
P 5100 2200
F 0 "#PWR07" H 5100 1950 50  0001 C CNN
F 1 "GNDREF" H 5300 2200 50  0001 C CNN
F 2 "" H 5100 2200 50  0001 C CNN
F 3 "" H 5100 2200 50  0001 C CNN
	1    5100 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR06
U 1 1 612E5EF2
P 3900 1750
F 0 "#PWR06" H 3900 1500 50  0001 C CNN
F 1 "GNDREF" H 3905 1577 50  0001 C CNN
F 2 "" H 3900 1750 50  0001 C CNN
F 3 "" H 3900 1750 50  0001 C CNN
	1    3900 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 1750 3550 1750
Wire Wire Line
	3250 1750 3250 2200
Wire Wire Line
	3250 2200 3150 2200
Connection ~ 3250 2200
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 61209EFE
P 7450 2750
F 0 "J2" H 7530 2742 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 7530 2651 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 7450 2750 50  0001 C CNN
F 3 "~" H 7450 2750 50  0001 C CNN
	1    7450 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 6131CEF8
P 4400 1700
F 0 "C4" V 4148 1700 50  0000 C CNN
F 1 "100nf" V 4239 1700 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 4438 1550 50  0001 C CNN
F 3 "~" H 4400 1700 50  0001 C CNN
	1    4400 1700
	0    1    1    0   
$EndComp
$Comp
L Device:C C5
U 1 1 6131F097
P 4400 2050
F 0 "C5" V 4250 2050 50  0000 C CNN
F 1 "10uf" V 4550 2050 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4438 1900 50  0001 C CNN
F 3 "~" H 4400 2050 50  0001 C CNN
	1    4400 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	4250 1700 4250 2050
Wire Wire Line
	4250 2050 4250 2150
Connection ~ 4250 2050
Connection ~ 4250 2150
Wire Wire Line
	4550 2050 4550 1700
$Comp
L power:GNDREF #PWR08
U 1 1 613232B5
P 4550 2050
F 0 "#PWR08" H 4550 1800 50  0001 C CNN
F 1 "GNDREF" H 4555 1877 50  0001 C CNN
F 2 "" H 4550 2050 50  0001 C CNN
F 3 "" H 4550 2050 50  0001 C CNN
	1    4550 2050
	1    0    0    -1  
$EndComp
Connection ~ 4550 2050
$Comp
L Graphic:Logo_Open_Hardware_Small #LOGO?
U 1 1 61336E30
P 7250 6800
F 0 "#LOGO?" H 7250 7075 50  0001 C CNN
F 1 "Logo_Open_Hardware_Small" H 7250 6575 50  0001 C CNN
F 2 "" H 7250 6800 50  0001 C CNN
F 3 "~" H 7250 6800 50  0001 C CNN
	1    7250 6800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
